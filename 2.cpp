#include <bits/stdc++.h>
using namespace std;
void swap(int *n,int *m)
{
	int tmp = *n;
	*n = *m;
	*m = tmp;
}
int main()
{
	int x,y,z;
	cin >> x >> y >> z;
	if(x>y)
	{
		swap(&x,&y);
	}
	if(x>z)
	{
		swap(&x,&z);
	}
	if(y>z)
	{
		swap(&y,&z);
	}
	cout << x << ' ' << y << ' ' << z;
	return 0;
}
