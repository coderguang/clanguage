#include <iostream>
using namespace std;
int a[100010];
int gcd(int n,int m)
{
	if(n%m == 0)
	{
		return m;
	}
	else
	{
		return gcd(m,n%m);
	}
}
int main()
{
	int t,n,i,cnt = 0;
	cin >> t;
	while(t--)
	{
		cin >> n;
		for(i = 1;i<1001;i++)
		{
			a[n/gcd(i,n)]++;
		}
			for(i = 0;i<100001;i++)
		{
			if(a[i]>0)
			{
				cnt++;
			}
		}
		cout << cnt << "\n";
	}
	
	return 0;
}
