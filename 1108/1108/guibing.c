#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
void merge(int tmparr[], int arr[], int l, int rorigin, int r)
{
	int left = l, right = r, origin = rorigin;
	int cnt = l;
	while (left < rorigin && origin < r + 1)
	{
		if (arr[left] < arr[origin])
		{
			tmparr[cnt++] = arr[left++];
		}
		else
			tmparr[cnt++] = arr[origin++];
	}
	while (left < rorigin)
	{
		tmparr[cnt++] = arr[left++];
	}
	while (origin < r + 1)
	{
		tmparr[cnt++] = arr[origin++];
	}
	for (int i = l; i<= r; i++)
	{
		arr[i] = tmparr[i];
	}
}
void separate(int tmparr[],int arr[], int l, int r)
{
	if (l < r)
	{
		separate(tmparr,arr,l, (l + r) / 2);
		separate(tmparr,arr,(l + r) / 2 + 1, r);
		merge(tmparr, arr, l, (l + r) / 2 + 1, r);
	}
}
void guibingsort(int arr[], int n)
{
	int* tmparr = (int*)malloc(n * sizeof(int));
	separate(tmparr,arr,0,n-1);
}
int main()
{
	int arr[] = { 4,1,8,23,7,3,8,9,12,5,6,10 };
	int n = sizeof(arr) / sizeof(arr[0]);
	guibingsort(arr, n);
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}