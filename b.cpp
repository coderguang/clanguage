#include <bits/stdc++.h>
using namespace std;
int n,m;
char a[5005][5005],b[5005],c[5005][5005]; 
struct pos
{
	int x,y;
};
queue<pos> q;
int X[4] = {-1,0,1,0};
int Y[4] = {0,1,0,-1};
int main()
{
	cin >> n >> m;
	int i,j,cnt = 0,x,y,sum = 0;
	bool flag = 1;
	getchar();
	for(i = 1;i<=n;i++)
	{
		for(j = 1;j<=m;j++)
		{
			a[i][j] = getchar();
		}
	}
	for(i = 1;i<=n;i++)
	{
		for(j = 1;j<=m;j++)
		{
			if(a[i][j]==' ')
			cnt++;
			if(a[i][j]==' '&&flag)
			{
				x = i;
				y = j;
				flag = 0;
			}
		}
	}
	pos tmp = {x,y};
	q.push(tmp);
	c[x][y] = 1;
	while(!q.empty())
	{
		sum++;
		int x1 = q.front().x;
		int y1 = q.front().y;
		q.pop();
		for(i = 0;i<4;i++)
		{
			int x2 = x1 + X[i];
			int y2 = y1 + Y[i];
			if(a[x2][y2] == ' '&&c[x2][y2] == 0)
			{
				c[x2][y2] = 1;
				pos tmp1 = {x2,y2};
				q.push(tmp1);
			}
		}
	}
	cout << cnt/sum;
	return 0;
}
