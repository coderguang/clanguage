#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void q_sort(int arr[], int n)
{
	if (n == 1||n==0)
		return 0;
	int tmp = arr[0];
	int left = 0;
	int right = n - 1;
	int f = 1;
	while (left < right)
	{
		if (f)
		{
			if (tmp > arr[right])
			{
				arr[left] = arr[right];
				f = !f;
			}
			else
				right--;
		}
		else
		{
			if (tmp < arr[left])
			{
				arr[right] = arr[left];
				f = !f;
			}
			else
				left++;
		}
	}
	arr[left] = tmp;
	q_sort(arr, left );
	q_sort(arr + left+1, n - left-1);
}
int main()
{
	int n = 1000;
	srand((unsigned int)time(NULL));
	int* arr = (int*)malloc(n * sizeof(int));
	for (int i = 0; i < n; i++)
	{
		arr[i] = rand()%1000+1;
		printf("%d ", arr[i]);
	}

	printf("\n");
	q_sort(arr, n);
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}

	return 0;
}